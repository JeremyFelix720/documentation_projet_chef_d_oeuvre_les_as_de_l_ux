# Oral de présentation de projet pour le titre CDA

## Objectifs de l'oral

L'oral se passe en 3 phases distinctes :
- Présentation du projet (40 min)
- Questions du jury (45 min)
- Echanges personnels (20 min)

Toutes ces phases se préparent et se travaillent pour être le plus à l'aise possible le jour J.
Mais votre diaporama doit être prêt pour la première phase
=> Présenter son projet de la même manière que votre dossier !

Je reprends donc le même plan que pour le dossier de projet :)

## Présentation du projet

### Introduction

Quel est l'objectif du projet pour le titre ?

Comment avez-vous eu l'idée du projet ou si c'est celui de votre binôme, expliquez la génèse, Comment s'est formé le groupe ?
Comment le projet a été adapté une fois le groupe formé?

Quelle est la partie du projet que vous avez préféré faire ?
Quelle difficulté avez-vous rencontré dans le projet ?
Quelle est la partie du projet dont vous êtes le plus fier ?
Quelle est la partie du projet que vous auriez aimé améliorer ?

Présentez les conditions que vous avez eu pour réaliser le projet (temps, ressources, compétences, planning, cours en lien avec le projet)

### Conception du projet

#### Première idée et évolutions du projet

Comment l'idée du projet a-t-elle été trifouillée jusqu'à devenir le projet final ?
Expliquez le plus précisément tout ce qui doit être fait !

#### Personas et user journeys

Copier coller et on explique ce que c'est et ce que ça a apporté

#### Wireframes et maquettes

Exemples et modifications apportées + maquettes + choix des couleurs

### Spécifications fonctionnelles

#### site map

On colle l'image si vous l'avez sinon tant pis...

#### MCD

On colle le MCD mais surtout on explique les relations, les tables, les champs...

#### Gestion de projet (Diagramme de Gantt et ou Kanban)

On colle et on explique le prévisionnel et comment on a prévu de gérer le projet dans le cas idéal prévu en début de projet

### Spécifications techniques

#### les technologies

React, Node, Java, PostgreSQL, C#, C++ surtout !!! etc... avec quelques logos svp ou une liste

#### l’architecture

Un beau schema avec des nuages, des carrés, des flèches et des couleurs
Miro peut aider ou si vous avez mieux => dans le channel adéquat

### Réalisation du projet

A vous de parler de ce que vous voulez. Regardez les compétences à valider et parler de celles que vous n'avez pas encore validées ^^

#### Développement

Présentation dans la code base directement des parties de code que vous avez réalisées et dont vous êtes fier + Lancement du projet ou mise en prod ou démo enregistrée selon votre capacité à montrer votre travail en condition réelle

#### Etapes / Avancées principales du projet

Comment le projet s'est passé et comment vous êtes vous améliorés au fur et à mesure du projet (individu + groupe)

### Conclusion

Estimez-vous avoir atteint l'objectif du projet ?
Qu'est-ce que vous auriez aimé faire de plus dans ce projet ?

Qu'avez-vous appris en réalisant ce projet ?
Qu'avez-vous appris sur vous-même en réalisant ce projet ?
Qu'avez-vous appris sur les autres en réalisant ce projet ?
Qu'avez-vous appris sur le travail en groupe en réalisant ce projet ?
Qu'avez-vous appris sur les différents métiers du numérique en réalisant ce projet ?

A quel point êtes vous fier de votre travail ?
Résumez votre situation en début d'année et comparez la à votre situation actuelle.
Quel est votre prochain objectif après ce projet ?

Trouvez une phrase qui résume votre projet professionnel et personnel (présent ou futur).

## Questions du jury

Pas de diapo ici ^^

Laissez vous guider par les questions du jury et répondez le plus précisément possible.
Vous pouvez anticiper vos points faibles pour les travailler et préparer des exemples (TP qui fonctionnent) ou des réponses à des questions pièges.

Pensez au référentiel et aux compétences à valider pour vous aider à préparer vos réponses. Vous devez être capable de justifier vos compétences en :
- création de projet
- architecture logicielle
- conception logicielle
- base de données + relations et requêtes
- API REST
- Authentification et autorisation (JWT)
- Développement front-end (HTML, CSS, React)
- Référencement (SEO)
- Déploiement (Vercel + Render)

## Echanges personnels

Pas de diapo ici ^^
On vous demandera sûrement ce que vous avez pensé de la formation, des formateurs, des intervenants, des projets, des cours, des TP, des examens, des évaluations.
Votre conclusion en soit mais en plus personnel :)
Insistez sur le fait que "Grâce au titre CDA, je vais pouvoir débuter mon travail chez ... où j'espère pouvoir mettre en pratique les compétences acquises lors de cette formation."