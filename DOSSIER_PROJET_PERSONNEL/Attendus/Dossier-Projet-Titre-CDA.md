# Dossier Projet - Titre CDA

## Remerciements

Merci Thomas !
Bon ok, merci Anthony, Christian, Leslie, Tiphaine, Guillaume, Patrice.
Et surtout, n'oubliez pas Thomas hein ^^
Jérémy, tu peux aussi remercier ton binôme, j'ai juste pas son prénom... :p

## Sommaire

Ben un sommaire quoi ... Automatique ce serait top !

## Résumé en anglais

Introduction + conclusion + https://www.deepl.com/fr/translator

Donc on écrit d'abord l'introduction et la conclusion en français !

## Introduction

Quel est l'objectif du projet pour le titre ?

Comment avez-vous eu l'idée du projet ou si c'est celui de votre binôme, expliquez la génèse, Comment s'est formé le groupe ?
Comment le projet a été adapté une fois le groupe formé?

Quelle est la partie du projet que vous avez préféré faire ?
Quelle difficulté avez-vous rencontré dans le projet ?
Quelle est la partie du projet dont vous êtes le plus fier ?
Quelle est la partie du projet que vous auriez aimé améliorer ?

Présentez les conditions que vous avez eu pour réaliser le projet (temps, ressources, compétences, planning, cours en lien avec le projet)

## Liste des compétences du référentiel

On copie colle et on met en page !

https://www.francecompetences.fr/recherche/rncp/31678/

```txt
Compétences attestées :
1. Concevoir et développer des composants d'interface utilisateur en intégrant les recommandations de sécurité
Maquetter une application.
Développer une interface utilisateur de type desktop.
Développer des composants d'accès aux données.
Développer la partie front-end d'une interface utilisateur web.
Développer la partie back-end d'une interface utilisateur web.
2. Concevoir et développer la persistance des données en intégrant les recommandations de sécurité
Concevoir une base de données.
Mettre en place une base de données.
Développer des composants dans le langage d'une base de données.
3. Concevoir et développer une application multicouche répartie en intégrant les recommandations de sécurité
Collaborer à la gestion d'un projet informatique et à l'organisation de l'environnement de développement.
Concevoir une application.
Développer des composants métier.
Construire une application organisée en couches.
Développer une application mobile.
Préparer et exécuter les plans de tests d'une application.
Préparer et exécuter le déploiement d'une application.
```

## Conception du projet

### Première idée et évolutions du projet

Comment l'idée du projet a-t-elle été trifouillée jusqu'à devenir le projet final ?
Expliquez le plus précisément tout ce qui doit être fait !

### Personas et user journeys

Copier coller et on explique ce que c'est et ce que ça a apporté

### Wireframes et maquettes

Exemples et modifications apportées + maquettes + choix des couleurs

## Spécifications fonctionnelles

### site map

On colle l'image si vous l'avez sinon tant pis...

### MCD

On colle le MCD mais surtout on explique les relations, les tables, les champs...

### Gestion de projet (Diagramme de Gantt et ou Kanban)

On colle et on explique le prévisionnel et comment on a prévu de gérer le projet dans le cas idéal prévu en début de projet

## Spécifications techniques

### les technologies

React, Node, Java, PostgreSQL, C#, C++ surtout !!! etc... avec quelques logos svp ou une liste

### l’architecture

Un beau schema avec des nuages, des carrés, des flèches et des couleurs
Miro peut aider ou si vous avez mieux => dans le channel adéquat

## Réalisation du projet

A vous de parler de ce que vous voulez. Regardez les compétences à valider et parler de celles que vous n'avez pas encore validées ^^

### Développement

Du code, des commits, des graphs de branches, des PR, des issues, des bugs, des features, des tests, des déploiements, des rollback, des merges, des conflits, des reviews, des refactors

### Améliorations et difficultés rencontrées dans le travail d'équipe

Pareil :
Du code, des commits, des graphs de branches, des PR, des issues, des bugs, des features, des tests, des déploiements, des rollback, des merges, des conflits, des reviews, des refactors

### Etapes / Avancées principales du projet

Pareil :
Du code, des commits, des graphs de branches, des PR, des issues, des bugs, des features, des tests, des déploiements, des rollback, des merges, des conflits, des reviews, des refactors

## Conclusion

Estimez-vous avoir atteint l'objectif du projet ?
Qu'est-ce que vous auriez aimé faire de plus dans ce projet ?

Qu'avez-vous appris en réalisant ce projet ?
Qu'avez-vous appris sur vous-même en réalisant ce projet ?
Qu'avez-vous appris sur les autres en réalisant ce projet ?
Qu'avez-vous appris sur le travail en groupe en réalisant ce projet ?
Qu'avez-vous appris sur les différents métiers du numérique en réalisant ce projet ?

A quel point êtes vous fier de votre travail ?
Résumez votre situation en début d'année et comparez la à votre situation actuelle.
Quel est votre prochain objectif après ce projet ?

Trouvez une phrase qui résume votre projet professionnel et personnel (présent ou futur).
