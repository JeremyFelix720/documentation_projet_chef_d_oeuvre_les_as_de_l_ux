# SAUVEGARDES DU CODE DES DIAGRAMMES DE GANTT EN LIGNE

## DIAGRAMME DE GANTT PREVISIONNEL

gantt
    title Diagramme de Gantt prévisionnel "Les As de l'UX"
    dateFormat YYYY-MM-DD

    section Sprint du 25 au 29/03/2024 (5j)

        Faire le diagramme de GANTT prévisionnel: 2024-03-25, 1d
        Gérer la connexion d'un utilisateur (sans stockage en BDD): 2024-03-25, 1d
        Gérer la création d'un compte via la BDD Sqlite (dans le backend): 2024-03-25, 1d

        Gérer la connexion d'un utilisateur (avec stockage en BDD): 2024-03-25, 2d
        Utiliser le local storage pour stocker des données propres à un compte: 2024-03-25, 2d

        Gérer la création d'un projet via la BDD: 2024-03-27, 1d
        Consommer / afficher les données des projets: 2024-03-27, 1d

        Gérer la publication d'un commentaire via la BDD: 2024-03-28, 1d
        Consommer / afficher les données des commentaires: 2024-03-28, 1d

        Gérer la selection de 5 commentaires par le dépositaire du projet: 2024-03-29, 1d
        Permettre au client de télécharger le PDF d'une page "analyse globale": 2024-03-29, 1d
        Faire le diagramme de GANTT effectif: 2024-03-29, 1d
        Faire le retroplanning: 2024-03-29, 1d


## DIAGRAMME DE GANTT EFFECTIF

