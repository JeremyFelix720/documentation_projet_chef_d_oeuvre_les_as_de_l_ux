# SAUVEGARDES DU CODE DES DIAGRAMMES DE GANTT EN LIGNE

## DIAGRAMME DE GANTT PREVISIONNEL

gantt
    title Diagramme de Gantt prévisionnel "Les As de l'UX"
    dateFormat YYYY-MM-DD

    section Sprint du 11 au 15/03/2024 (5j)

        Faire le diagramme de GANTT prévisionnel: 2024-03-11, 1d
        Synchroniser les branches tests_e2e", "dockerisation": 2024-03-11, 1d
        Intégrer le framework css Bootstrap en version CDN: 2024-03-11, 1d

        Faire le tri dans les pages dans le fichier des routes: 2024-03-12, 1d
        Ajouter les titres hiérarchisés partout: 2024-03-12, 1d
        Implémenter les séparateurs simples: 2024-03-12, 1d
        Ajouter les paragraphes simples partout: 2024-03-12, 1d
        Implémenter le footer: 2024-03-12, 1d

        Implémenter le formulaire: 2024-03-13, 2d
        Implémenter les inputs de formulaire: 2024-03-13, 2d
        Implémenter les menus déroulants: 2024-03-13, 2d
        Implémenter les checkbox: 2024-03-13, 2d
        Implémenter les boutons radio: 2024-03-13, 2d

        Implémenter les cartes projets: 2024-03-14, 1d
        Implémenter les toasts: 2024-03-14, 1d
        Implémenter les caroussels: 2024-03-14, 1d

        Ajouter la téléversation des images: 2024-03-15, 1d
        Rendre la connexion de l'utilisateur possible depuis la page dédiée: 2024-03-15, 1d
        Faire le diagramme de GANTT effectif: 2024-03-15, 1d
        Faire le rétroplanning: 2024-03-15, 1d


## DIAGRAMME DE GANTT EFFECTIF

gantt
    title Diagramme de Gantt effectif "Les As de l'UX"
    dateFormat YYYY-MM-DD

    section Sprint du 11 au 15/03/2024 (5j)

        Faire le diagramme de GANTT effectif: 2024-03-11, 1d
        
        Intégrer le framework CSS Bootstrap en version CDN: 2024-03-12, 1d
        Synchroniser les branches tests_e2e", "dockerisation": 2024-03-12, 1d
        Faire le tri dans les pages dans le fichier des routes: 2024-03-12, 1d

        Ajouter les titres hiérarchisés partout: 2024-03-13, 1d
        Implémenter les accordéons: 2024-03-13, 1d

        Ajouter les paragraphes simples partout: 2024-03-14, 1d
        Implémenter les menus accordéon: 2024-03-14, 1d

        Faire le diagramme de GANTT effectif: 2024-03-15, 1d
        Faire le rétroplanning: 2024-03-15, 1d