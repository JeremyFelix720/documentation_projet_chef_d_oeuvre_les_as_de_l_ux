# SAUVEGARDES DU CODE DES DIAGRAMMES DE GANTT EN LIGNE

## DIAGRAMME DE GANTT PREVISIONNEL

gantt
    title Diagramme de Gantt prévisionnel "Les As de l'UX"
    dateFormat YYYY-MM-DD

    section Sprint du 15 au 19/01/2024 (5j)
        Tableau Kanban: 2024-01-15, 1d
        Modèle Conceptuel de Données: 2024-01-15, 1d

        Modèle relationnel: 2024-01-16, 1d
        Diagramme de GANTT prévisionnel: 2024-01-16, 1d
        Script de création de la BDD: 2024-01-16, 1d
        Mise en place de la base de données: 2024-01-16, 1d

        Créer les routes d'API (authentification users + CRUD users / projects): 2024-01-17, 1d

        Intégration de la page "connexion" (frontend): 2024-01-18, 1d
        Tri dans le routing des pages (frontend): 2024-01-18, 1d
        Implémentation des pages (frontend): 2024-01-18, 2d

        Diagramme de GANTT effectif: 2024-01-19, 1d
        Retroplanning: 2024-01-19, 1d


## DIAGRAMME DE GANTT EFFECTIF

gantt
    title Diagramme de Gantt effectif "Les As de l'UX"
    dateFormat YYYY-MM-DD

    section Sprint du 15 au 19/01/2024 (5j)
        Tableau Kanban: 2024-01-15, 1d
        Cahier des Charges: 2024-01-15, 1d

        Diagramme de GANTT prévisionnel: 2024-01-16, 1d
        Modèle Conceptuel de Données: 2024-01-16, 2d

        Modèle relationnel: 2024-01-17, 1d
        Script de création de la BDD: 2024-01-17, 1d

        Veille sur les lois de l'UX Design: 2024-01-18, 1d
        Mise en place de la base de données (sur dbSqlite): 2024-01-18, 1d

        CRUD de la création d'un utilisateur (tâche non terminée): 2024-01-19, 1d